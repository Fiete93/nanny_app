# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

User.create!(firstname: "Example",
             name:  "User",
             email: "example@railstutorial.com",
             password:              "foobar",
             password_confirmation: "foobar",
             admin: true)

User.create!(firstname:  "Stefan",
             name:  "Helber",
             email: "stefan.helber@prod.uni-hannover.com",
             password:              "geheim",
             password_confirmation: "geheim",
             admin: true)



Nanny.create!(firstname: "Frau",
              name: "Keludowig")

Nanny.create!(firstname: "Heinz Harald",
              name: "Frentzen")
