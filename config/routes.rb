Rails.application.routes.draw do

  resources :nannies
  root             'static_pages#home'
  get 'Hilfe'    => 'static_pages#Hilfe'
  get 'Über'   => 'static_pages#Über'
  get 'Kontakt' => 'static_pages#Kontakt'
  get 'signup'  => 'users#new'
  get    'login'   => 'sessions#new'
  post   'login'   => 'sessions#create'
  delete 'logout'  => 'sessions#destroy'

  get 'Nannysuche', to: 'static_pages#Nannysuche'
  get 'Nanny-Angebot', to: 'static_pages#Nanny-Angebot'
  get 'Willkommen', to: 'static_pages#Willkommen'


  resources :users
end
